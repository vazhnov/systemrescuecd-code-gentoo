## Description

This is just clone of https://sourceforge.net/p/systemrescuecd/code/ci/master/tree/ — latest version based on Gentoo, 5.3.2.

## Another clones/mirrors/forks

* https://github.com/brulzki/sysrcd-gentoo — fork with own scripts;
* https://github.com/NiKiZe/systemrescuecd — copy of sysresccd-4.8.0, latest commit at Jul 1, 2016

## Another Gentoo-based live CD

* [CloverOS](https://gitgud.io/cloveros/cloveros/-/blob/master/livecd_build_minimal.sh) project has scripts to build 3 types of LiveCD
